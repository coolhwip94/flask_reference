# Flask Reference
> This repo will host references and examples for flask, flask-restplus and rest API related information


### Running example api
---
```
python3 -m venv venv
source venv/bin/acitvate

pip3 install -r requirements.txt
python3 my_api.py
```

