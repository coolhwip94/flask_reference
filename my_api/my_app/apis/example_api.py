from flask_restplus import Namespace, Resource


api = Namespace('v1/example_api', description='example api')


@api.route("/")
@api.response(404, 'Page Not Found')
@api.response(200, 'OK')
class Example(Resource):
    @api.doc(
        
        responses={
            200: 'Ok',
            201: 'Created',
            400: 'Bad Request'},)
    # validate true will return 400 if value not in payload
    def get(self):
        return "example return"
