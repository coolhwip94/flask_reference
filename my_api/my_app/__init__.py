from flask import Flask, Blueprint
from flask_restplus import Api

from .apis.example_api import api as example_api

__version__ = "1.0.0"
app = Flask(__name__)
authorizations = {
    'Basic Auth': {
        'type': 'basic',
        'in': 'header',
        'name': 'Authorization'
    },
}


# to serve swagger libs
blueprint = Blueprint('api', __name__)

# All API metadata
api = Api(
    blueprint,
    title='my api',
    version=__version__,
    description='simple flask reference',
    security='Basic Auth',
    authorizations=authorizations,
    catch_all_404s=True
)

app.register_blueprint(blueprint)
app.config['ERROR_404_HELP'] = False
api.add_namespace(example_api)
